import picamera
import commands
import time
import random
#from PIL import Image
#import numpy as np


def capturarImagen():
	nombreFile = ""
	camara = picamera.PiCamera()
	try:
		nombreFile = "imgs/imagen" + time.strftime("%y%m%d_%H%M%S") + ".png"

		camara.capture(nombreFile)
		camara.close()
	except:
		camara.close()
	return nombreFile


def evaluarFrambuesa(imagen):
	# print("Evaluando imagen " + imagen)
	comando = "frambuC/./main " + imagen
	resp = commands.getoutput(comando)
	return resp


def evaluarFrambuesa2(imagen):
	resp = ""
	for i in range(0, 6):

		if i < 5:
			resp = resp + str(random.randrange(100))
			resp = resp + ";"
		else:
			resp = resp + str(random.randrange(500))
	# print(resp)
	return resp


def aplicarTransform(imagen, vector):
	return True
	#img = Image.open(imagen)
	#arr = np.array(img)

	#for i in range(arr.len):
		#for j in range(arr[i].len):
			#for k in range(0, vector.len):
				#if (arr[i, j, k] + vector[k] > 255):
					#arr[i, j, k] = 255
				#elif (arr[i, j, k] + vector[k] < 0):
					#arr[i, j, k] = 0
				#else:
					#arr[i, j, k] += vector[k]
