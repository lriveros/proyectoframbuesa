#!/usr/bin/python
# -*- coding: utf8 -*-
try:
	#from Tkinter import *  # Python 2
	import Tkinter as tk
except ImportError:
	#from tkinter import *  # Python 3
	import tkinter as tk

import ttk
import ConfigParser
import thread
import utils
import time


fuente = "Courier 10 bold"
valorIqf = int(75)

flagCalibra = False

class MainWindow(tk.Frame):

	def __init__(self, *args, **kwargs):
		tk.Frame.__init__(self, *args, **kwargs)

		self.txtBtnIniciar = tk.StringVar()
		self.txtBtnIniciar.set("Iniciar")

		self.txtReBlock = tk.StringVar()
		self.txtReIqf = tk.StringVar()
		self.txtReClase = tk.StringVar()

		self.txtReBlock.set("- %")
		self.txtReIqf.set("- %")
		self.txtReClase.set(" - ")

		self.lblBlock = tk.Label(self, text="% Calidad Block :", font=fuente)
		self.lblIqf = tk.Label(self, text="% Calidad IQF   :", font=fuente)
		self.lblClasificacion = tk.Label(self, text="Clasificación   :", font=fuente)

		self.lblReBlock = tk.Label(self, textvariable=self.txtReBlock, font=fuente)
		self.lblReIqf = tk.Label(self, textvariable=self.txtReIqf, font=fuente)
		self.lblReClase = tk.Label(self, textvariable=self.txtReClase.get(), font=fuente)

		self.pbClasificacion = ttk.Progressbar(orient='vertical', mode='determinate', maximum=100)

		self.btnIniciar = tk.Button(self, textvariable=self.txtBtnIniciar, command=self.iniciarProceso)

		self.btnIniciar.pack(side="top", fill="both", expand=False, padx=80, pady=15)
		#self.btnIniciar.pack(pady=15)

		self.lblBlock.pack(side="top", fill="both", expand=True, padx=0, pady=80)
		#self.lblBlock.place(x=10, y=80)
		self.lblIqf.place(x=10, y=60)
		self.lblClasificacion.place(x=10, y=110)
		self.lblReBlock.place(x=150, y=80)
		self.lblReIqf.place(x=150, y=60)
		self.lblReClase.place(x=150, y=110)
		self.pbClasificacion.place(x=200, y=60)

	def calibrar(self):
		## Crea la ventana hija.
		self.t1 = tk.Toplevel(root)

		self.t1.geometry('225x130+20+40')
		self.t1.wm_title("Calibrando . . .")
		self.t1.deiconify()
		## Provoca que la ventana tome el focus
		self.t1.focus_set()
		## Deshabilita todas las otras ventanas hasta que
		## esta ventana sea destruida.
		self.t1.grab_set()
		## Indica que la ventana es de tipo transient, lo que significa
		## que la ventana aparece al frente del padre.
		self.t1.transient(master=root)

		self.txtOk1 = tk.StringVar()
		self.txtOk2 = tk.StringVar()
		self.txtOk3 = tk.StringVar()

		self.txtOk1.set("")
		self.txtOk2.set("")
		self.txtOk3.set("")

		self.lblImgn = tk.Label(self.t1, text="Imagen calibración.....", font=fuente)
		self.lblVect = tk.Label(self.t1, text="Calculando vectores....", font=fuente)
		self.lblCaTo = tk.Label(self.t1, text="Estado de calibración..", font=fuente)
		self.lblOkImgn = tk.Label(self.t1, textvariable=self.txtOk1, font=fuente)
		self.lblOkVect = tk.Label(self.t1, textvariable=self.txtOk2, font=fuente)
		self.lblOkCaTo = tk.Label(self.t1, textvariable=self.txtOk3, font=fuente)
		self.lblImgn.place(x=5, y=10)
		self.lblVect.place(x=5, y=30)
		self.lblCaTo.place(x=5, y=50)
		self.lblOkImgn.place(x=180, y=10)
		self.lblOkVect.place(x=180, y=30)
		self.lblOkCaTo.place(x=180, y=50)

		thread.start_new_thread(self.calcularTransformacion, ())


		## Pausa el mainloop de la ventana de donde se hizo la invocación.
		self.t1.wait_window(self.t1)
		# root.wait_window(t1)

	def calcularTransformacion(self):
		print("Calcuar transformacion")
		time.sleep(1)
		self.txtOk1.set("OK")
		print("Calcuar transformacion1")
		time.sleep(1)
		self.txtOk2.set("OK")
		print("Calcuar transformacion2")
		time.sleep(1)
		self.txtOk3.set("OK")
		print("Calcuar transformacion3")
		time.sleep(1)
		print("Calcuar transformacion fin")
		self.t1.destroy()
		flagCalibra = True
		print("Calcuar transformacion fin1")

	def iniciarProceso(self):
		self.calibrar()
		print("Detener Proceso")

if __name__ == "__main__":
	root = tk.Tk()
	root.title("Evaluador Frambuesa")
	root.geometry("256x192+5+5")
	main = MainWindow(root)
	main.pack(side="top", fill="both", expand=False)
	root.mainloop()