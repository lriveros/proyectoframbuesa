/*
    * Archivo "cabeceraControl.h"
    * autor Luis C. Riveros Díaz - Universidad Católica del maule
    * Julio de 2013
    *
    * libreria creada exclusivamente para el proyecto Estimación de madurez de la frambuesa
    * utilizando técnicas de procesamiento de imágenes digitales
    * desarrollado en Agrofreeze LTDA, Talca, Region del Maule, Chile
    *
*/
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int (*scale)[3];
int ** representativeColors;
int numberOfColors;
int ** auxCopy;
int * lvlsInScale;
int aux[3];


/*
    * En el archivo scale.prf se almacenara la escala de color
    * en donde la priemra linea contiene el numero de colores de la escala
    * y las siguientes lineas contienen 3 numeros correspondientes a
    * los colores en espacio de color RGB
    *   La ecala de color es una matriz de Nx3
    *
*/
int ctrlDefColorScale(){
    FILE *fil=fopen("scale.pfr","r");
    int i,j;

    if(!fil)
        return 0;

    fscanf(fil,"%d\n",&numberOfColors);
    scale = malloc(numberOfColors*3*sizeof(int));
    for(i=0;i<numberOfColors;i++)
        for(j=0;j<3;j++)
            fscanf(fil,"%d ",&scale[i][j]);
    fclose(fil);
    return 1;
}

/*
    * Retorna en base a un color, el nivel que le corresponde en la escala
    * los niveles van desde 1-N , con N el numero de colores de tiene la escala
*/
int ctrlAssignColorLvl(int rgbColor[3]){
    int lvlMin,i;
    double dist,minDist;
    ctrlDefColorScale();
    minDist=sqrt(pow((rgbColor[0]-scale[0][0]),2)+pow((rgbColor[1]-scale[0][1]),2)+pow((rgbColor[2]-scale[0][2]),2));
    lvlMin=0;
    for(i=1;i<numberOfColors;i++){
        dist=sqrt(pow((rgbColor[0]-scale[i][0]),2)+pow((rgbColor[1]-scale[i][1]),2)+pow((rgbColor[2]-scale[i][2]),2));
        if(dist<minDist){
            minDist=dist;
            lvlMin=i;
        }
    }
    return lvlMin;
}

/*
    * Muestra los colores de la escala de color
    * almacenados en el archivo scale.prf
    *
*/
void ctrlShowScale(){
    int i,j;
    for(i=0;i<numberOfColors;i++){
        for(j=0;j<3;j++){
            printf("%d ",scale[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}


void ctrlQuickSort(int m, int n ){
    int i,j,k,temp,key;

    if( m < n){

        //elegir el pivote
        k = (m+n)/2;

        ////swap
        //printf("here\n");
        aux[0]=auxCopy[m][0];
        aux[1]=auxCopy[m][1];
        aux[2]=auxCopy[m][2];
        auxCopy[m][0]=auxCopy[k][0];
        auxCopy[m][1]=auxCopy[k][1];
        auxCopy[m][2]=auxCopy[k][2];
        auxCopy[k][0]=aux[0];
        auxCopy[k][1]=aux[1];
        auxCopy[k][2]=aux[2];

        key = auxCopy[m][0];
        i = m+1;
        j = n;
        while(i <= j){
            while((i <= n) && (auxCopy[i][0]<= key))
                i++;
            while((j >= m) && (auxCopy[j][0] > key))
                j--;
            if(i<j){
                aux[0]=auxCopy[i][0];
                aux[1]=auxCopy[i][1];
                aux[2]=auxCopy[i][2];
                auxCopy[i][0]=auxCopy[j][0];
                auxCopy[i][1]=auxCopy[j][1];
                auxCopy[i][2]=auxCopy[j][2];
                auxCopy[j][0]=aux[0];
                auxCopy[j][1]=aux[1];
                auxCopy[j][2]=aux[2];
            }
        }
        ////swap
        aux[0]=auxCopy[m][0];
        aux[1]=auxCopy[m][1];
        aux[2]=auxCopy[m][2];
        auxCopy[m][0]=auxCopy[j][0];
        auxCopy[m][1]=auxCopy[j][1];
        auxCopy[m][2]=auxCopy[j][2];
        auxCopy[j][0]=aux[0];
        auxCopy[j][1]=aux[1];
        auxCopy[j][2]=aux[2];


        ctrlQuickSort(m,j-1);
        ctrlQuickSort(j+1,n);
    }
}

/*
    * En base a un segmento seleccionado, se obtiene el color
    * representativo y se almacena en el arreglo de destino
*/
void ctrlRepresentative(int size,int index){
    int median;
    median=size/2;

    ctrlQuickSort(0,size-1);

    representativeColors[index][0]=auxCopy[median][0];
    representativeColors[index][1]=auxCopy[median][1];
    representativeColors[index][2]=auxCopy[median][2];
}

/*
    * Recibe un arreglo de nx3, n cantidad de colores de la imagen,
    * y el numero de colores representativos que se desea obtener
*/
int** ctrlPartitions(int** segmentedColors,int nColors,int nParts){
    int sizeParts,i,j;

    sizeParts=nColors/nParts;

    representativeColors= (int **)malloc(sizeof(int *)*nParts);
    lvlsInScale= (int *)malloc(sizeof(int )*nParts);
    for(i=0;i<nParts;i++)
		representativeColors[i] = (int *)malloc(sizeof(int)*3);    //arreglo de salida con los colres representativos finales

    auxCopy=(int **)malloc(sizeof(int *)*sizeParts);
    for(i=0;i<sizeParts;i++)                         // arreglo de transcicion de los subconjuntos de los que se debe obtener
        auxCopy[i]=(int *)malloc(sizeof(int)*3);
                                                                    // los colores representativos
    for(i=0;i<nParts;i++){
        for(j=0;j<sizeParts-1;j++){

            auxCopy[j][0]=segmentedColors[i*sizeParts+j][0];
            auxCopy[j][1]=segmentedColors[i*sizeParts+j][1];
            auxCopy[j][2]=segmentedColors[i*sizeParts+j][2];
        }
        ctrlRepresentative(sizeParts,i);
    }
    return representativeColors;
}
///////////////////////////////////////////////DESDE AQUI COMIENZA EL METODO DE LOS MINIMOS CUADRADOS

void PideDatos(int *Dat, int *Ord,float Val[][102])
{
    int A,B;

    printf("\n\n\n METODO DE MINIMOS CUADRADOS.\n\n");
    printf("\n Introduce el numero de datos (Puntos): ");scanf("%d",&*Dat);
    printf("\n\n\n Introce los valores de cada punto\n");

    for(A=1;A<=*Dat;A++)
    {
        printf(" -Valores del Punto %d:\n",A);
        printf("   X%d: ",A); scanf("%f",&Val[0][A]);
        printf("   Y%d: ",A); scanf("%f",&Val[1][A]);
    }
    printf("\n\n\n Introduce el orden del polinomio: "); scanf("%d",&B);
    *Ord=B+1;
}

float Potencia(int n, float Num)
{
    int A;
    float res;

    res=1;
    for(A=1;A<=n;A++) res=res*Num;
    return res;
}

void ResuelveGauss(int Dim, float Sist[][102])
{
    int NoCero,Col,C1,C2,A;
    float Pivote,V1;

    for(Col=1;Col<=Dim;Col++){
        NoCero=0;A=Col;
        while(NoCero==0){
            if(Sist[A][Col]!=0){
                NoCero=1;}
            else A++;}
        Pivote=Sist[A][Col];
        for(C1=1;C1<=(Dim+1);C1++){
            V1=Sist[A][C1];
            Sist[A][C1]=Sist[Col][C1];
            Sist[Col][C1]=V1/Pivote;}
        for(C2=Col+1;C2<=Dim;C2++){
            V1=Sist[C2][Col];
            for(C1=Col;C1<=(Dim+1);C1++){
                Sist[C2][C1]=Sist[C2][C1]-V1*Sist[Col][C1];}
        }}

    for(Col=Dim;Col>=1;Col--) for(C1=(Col-1);C1>=1;C1--){
        Sist[C1][Dim+1]=Sist[C1][Dim+1]-Sist[C1][Col]*Sist[Col][Dim+1];
        Sist[C1][Col]=0;
    }
}

void PreparaSistema(int Ord, int Dat, float Sist[][102], float Val[][102]){
    int A,B,C,Exp;
    float suma,termino;

    for(A=1;A<=Ord;A++)    for(B=1;B<=Ord;B++)
    {
        suma=0;
        Exp=A+B-2;

        for(C=1;C<=Dat;C++)
        {
            termino=Val[0][C];
            suma=suma+Potencia(Exp,termino);
        }
        Sist[A][B]=suma;
    }
    for(A=1;A<=Ord;A++)
    {
        suma=0;
        Exp=A-1;

        for(C=1;C<=Dat;C++)
        {
            termino=Val[0][C];
            suma=suma+Val[1][C]*Potencia(Exp,termino);
        }
        Sist[A][Ord+1]=suma;
    }
}

void EscribeDatos(int Dim, float Sist[][102])
{
    int A,B;
    printf("\n\n");
    for(A=1;A<=Dim;A++){
        for(B=1;B<=(Dim+1);B++){
            printf("%7.2f",Sist[A][B]);
            if(B==Dim) printf("   |");}
        printf("\n");
    }
    printf("\n\n");
}

void ctrlMinimos(){
    int Datos,Orden,C;
    float Valores[2][102],Sistema[102][102];
    PideDatos(&Datos,&Orden,Valores);
    PreparaSistema(Orden,Datos,Sistema,Valores);
    //printf("\n\n El sistema a resolver es el siguiente:");
    EscribeDatos(Orden,Sistema);
    ResuelveGauss(Orden,Sistema);
    //printf("\n\n El sistema resuelto:");
    EscribeDatos(Orden,Sistema);
    printf("\n\n La Ecuacion del Polinomio ajustado por minimos Cuadrados\n\n:");
    for(C=1;C<=Orden;C++)
        printf(" + (%f)X^%d",Sistema[C][Orden+1],C-1);
}















