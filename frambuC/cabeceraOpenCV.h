/*
    * Archivo "cabeceraOpenCV.h"
    * autor Luis C. Riveros Díaz - Universidad Católica del maule
    * Julio de 2013
    *
    * libreria creada exclusivamente para el proyecto Estimación de madurez de la frambuesa
    * utilizando técnicas de procesamiento de imágenes digitales
    * desarrollado en Agrofreeze LTDA, Talca, Region del Maule, Chile
    *
    * instalación de openCV necesaria para compilar
    *
*/

#include "opencv/highgui.h"
#include <stdio.h>
#include <stdlib.h>
#include <cv.h>


int rows,columns;
long tiempo=0;
int nColorSeg=0;
int ** matrixR;
int ** matrixG;
int ** matrixB;
int ** matrixBW;
int ** segmentedColors;
IplImage* image;
IplImage* imageBW;
uchar *ptr_imgn;
uchar *ptr_imgnAux;


/*  *
    * Método que abre la imagen y genera 3 matrices de dimensiones R, G y B
    * además completa los campos rows y columns correspondientes a las
    * dimensiones de la imagen ingresada
    *
    * Recordar que en la imagen, el orden es [columnas, filas]
    *
*/
void ocvReadImage(char* imageName){
    //Leer imagen y obtener su tamaño
	int i,j;
	CvScalar s;
	image=(IplImage *)cvLoadImage(imageName, 1);   //1 porque la imagen se encuentra en colores
	imageBW=(IplImage *)cvLoadImage(imageName, 0); //0 para la imagen en escala de grises
	rows=image->height;
	columns=image->width;

	//generar el tamaño de la matriz en base a la imagen
	matrixR = (int **)malloc(sizeof(int *)*rows);
	matrixG = (int **)malloc(sizeof(int *)*rows);
	matrixB = (int **)malloc(sizeof(int *)*rows);
	matrixBW= (int **)malloc(sizeof(int *)*rows);
	for(i=0;i<rows;i++){
		matrixR[i] = (int *)malloc(sizeof(int)*columns);
        matrixG[i]= (int *)malloc(sizeof(int)*columns);
        matrixB[i] = (int *)malloc(sizeof(int)*columns);
		matrixBW[i] = (int *)malloc(sizeof(int)*columns);
	}

	for(i=0;i<rows;i++){
        ptr_imgn = (uchar*)(imageBW->imageData + i * imageBW->widthStep);
        for(j=0;j<columns;j++){
                matrixBW[i][j] = ptr_imgn[j];
        }
    }

    for(i=0;i<rows;i++){
        for(j=0;j<columns;j++){
            s = cvGet2D(image,i,j);
            matrixR[i][j]=(int)s.val[2];
            matrixG[i][j]=(int)s.val[1];
            matrixB[i][j]=(int)s.val[0];
        }
    }

    //printf("\nR:%d  G:%d  B:%d\n",matrixR[370][199],matrixG[370][199],matrixB[370][199]);
    //ocvShowPixel();
}

/*  *
    * Utilizando una ventana de openCV, se muestra la imagen que se esta trabajando
    *
*/
void ocvShowImage(char* imageName){
	cvNamedWindow(imageName, CV_WINDOW_AUTOSIZE );
    cvShowImage(imageName, image );
    cvWaitKey(0);
    cvReleaseImage( &image );
    cvDestroyWindow(imageName);
}

void ocvShowImageBW(char* imageName){
    cvNamedWindow(imageName, CV_WINDOW_AUTOSIZE );
    cvShowImage(imageName, imageBW );
    cvWaitKey(0);
    cvReleaseImage( &imageBW );
    cvDestroyWindow(imageName);
}

/*  *
    * Segmenta la imagen de estudio en base a un umbral determinado
    *
*/
void ocvThresholdBW(){
    int threshold=100;
    int i,j;
    for(i=0;i<rows;i++){
        for(j=0;j<columns;j++){
            if(matrixBW[i][j]<=threshold){
                matrixBW[i][j]=255;
                nColorSeg++;
            }else{
                matrixBW[i][j]=0;
            }
        }
    }
}

void ocvShowSegmentedImage(){
    int i,j;
	for(i=0;i<rows;i++){
        ptr_imgnAux = (uchar*)(imageBW->imageData + i * imageBW->widthStep);
        for(j=0;j<columns;j++){
            ptr_imgnAux[j]=matrixBW[i][j];
        }
    }
    cvNamedWindow( "Imagen Segmentada", CV_WINDOW_AUTOSIZE );
    cvShowImage( "Imagen Segmentada", imageBW );
    cvWaitKey(0);
    cvReleaseImage( &imageBW );
    cvDestroyWindow("Imagen Segmentada" );
}

ocvShowMatrixBW(){
    int i,j;
    for(i=0;i<rows;i++){
        for(j=0;j<columns;j++){
            printf("%d ",matrixBW[i][j]);
        }
        printf("\n");
    }
}

/*  *
    * Guarda la imagen en el directorio con el nombre que se indica
    *
*/
void ocvSaveImage(char* imageName){
    int p[3];
    p[0] = CV_IMWRITE_JPEG_QUALITY;
    p[1] = 10;
    p[2] = 0;
    cvSaveImage(imageName, image,p);
}

/*  *
    * Retorna el número de filas de la imagen abierta
    *
*/
int ocvGetRows(){
    return rows;
}

/*  *
    * Retorna el número de columnas de la imagen abierta
    *
*/
int ocvGetColumns(){
    return columns;
}

int** ocvGetR(){
    return matrixR;
}

int** ocvGetG(){
    return matrixG;
}

int** ocvGetB(){
    return matrixB;
}

int ocvGetNColorSeg(){
    return nColorSeg;
}

/*
    *Entrega una matriz de nx3 con los colores
*/
int** ocvGetSegmentedColors(){
    int i,j,contSC=0;

    segmentedColors= (int **)malloc(sizeof(int *)*nColorSeg);
	for(i=0;i<nColorSeg;i++)
		segmentedColors[i] = (int *)malloc(sizeof(int)*3);


    for(i=0;i<rows;i++){
        for(j=0;j<columns;j++){
            //printf("i=%d j=%d colores: %d rows: %d columns: %d numColor: %d  matrizBW: %d   matrixR : %d  largoSegmentedColors: \n",i,j,nColorSeg,rows,columns,contSC,matrixBW[i][j],matrixR[i][j]);
            if (matrixBW[i][j]==255){
                //printf("--- %d",matrixR[i][j]);
                segmentedColors[contSC][0]=matrixR[i][j];
                //printf(" - %d-",segmentedColors[contSC][0]);
                segmentedColors[contSC][1]=matrixG[i][j];
                //printf(" - %d- ",segmentedColors[contSC][1]);
                segmentedColors[contSC][2]=matrixB[i][j];
                //printf(" - %d-\n",segmentedColors[contSC][2]);

                contSC++;
            }
        }
    }

    return segmentedColors;
}
