//compilar gcc -fopenmp ma.c -o test `pkg-config --cflags --libs opencv`

#include <stdio.h>
#include "prueba.h"
#include "ordenar.h"
#include "cabeceraOpenCV.h"
#include "cabeceraControl.h"

int f=9;
int c=3;

int main(int argc, char** argv){
    int SUM,filas,columnas;
    int mat[9][3]={{1,6,3},{9,8,7},{1,2,3},{7,8,1},{3,4,7},{4,2,8},{8,1,2},{7,8,9},{5,7,6}};
    int color[3]={20,15,22};
    //SUM=sumar(3,5);
    //printf("%d\n",SUM);
    defMat(9,3);
    defDimension(9,3);
    mostrarArreglo(mat);
    printf("\t---\n");
    mat[9][3]=burbujaP(mat);
    mostrarArreglo(mat);
    printf("\t---\n");

    ocvReadImage(argv[1]);
    filas=ocvGetRows();
    columnas=ocvGetColumns();
    printf("La imagen tiene %d filas y %d columnas\n", ocvGetRows(),ocvGetColumns());
    //ocvShowImage("Muestra");
    //ocvSaveImage("Test,jpg");
    leer2(argv[1]);
    ///test de cabeceraControl.h
    if(!ctrlDefColorScale()){

        exit(EXIT_FAILURE);
    }
    ctrlShowScale();
    printf("El color es asignado al nivel %d de la escala\n",ctrlAssignColorLvl(color));
    ///fin de test cabeceraControl.h
}
