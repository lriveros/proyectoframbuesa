#bin/bash

# A Script wich compiles OpenCV on the Raspberry Pi
# (c) Matthias Riegler, 2012

# install cmake on the Pi
echo "install cmake"
sudo apt-get install cmake


# download and uncompress OpenCV tarball

mkdir /tmp/build_opencv
cd /tmp/build_opencv

echo "download OpenCV Tarball"

#wget http://downloads.sourceforge.net/project/opencvlibrary/opencv-unix/2.4.3/OpenCV-2.4.3.tar.bz2
wget https://src.fedoraproject.org/repo/pkgs/opencv/OpenCV-2.4.3.tar.bz2/md5/c0a5af4ff9d0d540684c0bf00ef35dbe/OpenCV-2.4.3.tar.bz2
echo "uncompress"

tar xjvf OpenCV-2.4.3.tar.bz2


# Build
cd OpenCV-2.4.3

echo "configure"
cmake CMakeLists.txt
echo "done"


echo "start build"
make
echo "done, install"
sudo make install

echo "done, thank you for using my script and have fun with opencv :)"
