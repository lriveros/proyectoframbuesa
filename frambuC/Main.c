/**
    * Archivo "Main.c"
    * autor Luis C. Riveros Díaz - Universidad Católica del maule
    * Julio de 2013
    *
    * libreria creada exclusivamente para el proyecto Estimación de madurez de la frambuesa
    * utilizando técnicas de procesamiento de imágenes digitales
    * desarrollado en Agrofreeze LTDA, Talca, Region del Maule, Chile
    *
    * instalación de openCV necesaria para compilar
    *
    * comando de instalación
    *       (linux) gcc -fopenmp Main.c -o main -lm `pkg-config --cflags --libs opencv`
    *       (windw) gcc Main.c -lopencv_core243 -lopencv_highgui243 -o main
*/

#include <stdio.h>
#include "cabeceraOpenCV.h"
#include "cabeceraControl.h"


#define numSegmentos 500

int ** matrixR;
int ** matrixG;
int ** matrixB;
int ** segmentedColors;
int ** repColors;
int * arrayDist;
int rows,cols,nColorSeg;

void getImageData(char* imageName);
void getRepresentativeColors();
void showFinalStats();

int main(int argc, char** argv){
    if(argc!=2){
        printf("\n Modo de uso ./main <nombreImagen>\n");
        exit(1);
    }
    //printf("\n\t----- Abriendo imagen \"%s\" -----\n",argv[1]);
    getImageData(argv[1]);
    getRepresentativeColors();
    showFinalStats();
}

void getImageData(char* imageName){
    ocvReadImage(imageName);                    // se lee la imagen a estudiar
    ocvThresholdBW();                           // se umbraliza la imagen para obtener una en blanco y negro

    //obteniendo los arreglos R, G y B
    matrixR=ocvGetR();
    matrixG=ocvGetG();
    matrixB=ocvGetB();

    //ocvShowImageBW(imageName);
    //ocvShowSegmentedImage();



    segmentedColors=ocvGetSegmentedColors();    // Se obtiene un arreglo de nx3 con los colores relevantes de la imagen (n=numero de colores)
    nColorSeg=ocvGetNColorSeg();                // se obtiene el numero de colores que compone el arreglo anterior


    //getRepresentativeColors();
    //printf("\nR:%d  G:%d  B:%d\n",matrixR[370][199],matrixG[370][199],matrixB[370][199]);
    //ocvShowMatrixBW();
    //rows=ocvGetRows();
    //cols=ocvGetColumns();
    //printf("\nFilas    : %d \nColumnas : %d\n",rows,cols);

}

void getRepresentativeColors(){
    int i=0;
    int aux[3];
    //printf("Numero de Colores: %d\n",nColorSeg);
    repColors=ctrlPartitions(segmentedColors,nColorSeg,numSegmentos);

    arrayDist = (int *)malloc(sizeof(int )*numSegmentos);
    for(i=0;i<numSegmentos;i++){
        aux[0]=repColors[i][0];
        aux[1]=repColors[i][1];
        aux[2]=repColors[i][2];
        arrayDist[i]=ctrlAssignColorLvl(aux);
        //printf("Color %d asignado a nuvel %d \n",i,arrayDist[i]);
    }
}

void showFinalStats(){
    int i,a,b;
    int contador[6]={0,0,0,0,0,0};
    float stats[6];
    for(i=0;i<numSegmentos;i++)
        contador[arrayDist[i]]++;

    //for(i=0;i<6;i++)
    //  printf("%d \n",contador[i]);


    //printf("\t\t --Grados de pertenencia de la imagen --\n");
    for(i=0;i<6;i++){
        stats[i]=(float)(((float)contador[i]/(float)numSegmentos)*100);
        //printf("Nivel %d -> %3.2f \n",i,stats[i]);
        printf("%d",contador[i]);
        if(i<5){
            printf(";");
        }
    }
    printf("\n");
    /*
    printf("Los niveles 4 y 5 corresponden a IQF\n");
    a=contador[0]+contador[1]+contador[2]+contador[3];
    b=contador[4]+contador[5];
        
    if(b>=a)
        printf("Por lo tanto la fruta es IQF\n");
    else
        printf("Por lo tanto la fruta es BLOCK\n");
    */
}


