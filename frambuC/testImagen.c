#include "opencv/highgui.h"
#include <stdio.h>
#include "prueba.h"

int fil,col;
int ** matrIn;
IplImage* imagen;
uchar *ptr_imgn;


void ocvShowPixel(){

    CvScalar s;
    printf("\n Accediendo a pixels de la imagen");
    s = cvGet2D(imagen,4,4);
    printf("\n Pixel ( 4,4 ) : (B=%.0f; G=%.0f; R=%.0f)",s.val[0], s.val[1], s.val[2]);
    s = cvGet2D(imagen,4,20);
    printf("\n Pixel (4,20) : (B=%.0f; G=%.0f; R=%.0f)",s.val[0], s.val[1], s.val[2]);
    s = cvGet2D(imagen, 20,4);
    printf("\n Pixel (20,4 ) : (B=%.0f; G=%.0f; R=%.0f)",s.val[0], s.val[1], s.val[2]);
    s = cvGet2D(imagen,40,40);
    printf("\n Pixel (40,40) : (B=%.0f; G=%.0f; R=%.0f\n)",s.val[0], s.val[1], s.val[2]);

}

void leerImgnMatriz(char* nombreImgn){
    //Leer imagen y obtener su tamaño
	int i,j;
	imagen=(IplImage *)cvLoadImage(nombreImgn, 1);
	fil=imagen->height;
	col=imagen->width;

	//generar el tamaño de la matriz en base a la imagen
	matrIn = (int **)malloc(sizeof(int *)*fil);
	for(i=0;i<fil;i++){
		matrIn[i] = (int *)malloc(sizeof(int)*col);
		//for(j=0;j<3;j++){
        //    matrIn[i][j]=(int )malloc(sizeof(int)*3);
		//}
	}
	//completar la matriz in con los datos de la imagen
	for(i=0;i<fil;i++){
        ptr_imgn = (uchar*)(imagen->imageData + i * imagen->widthStep);
        for(j=0;j<col;j++){
                matrIn[i][j] = ptr_imgn[j];
        }
    }

    printf("\n%d %d\n",col,matrIn[384][1258]);
    //defMat(fil,col);
    //verMatriz(matrIn);
    //mostrarArreglo(matrIn);

    ocvShowPixel();

    cvNamedWindow( "Frambuesa de Muestra", CV_WINDOW_AUTOSIZE );
    cvShowImage( "Frambuesa de Muestra", imagen );
    cvWaitKey(0);
    cvReleaseImage( &imagen );
    cvDestroyWindow("Frambuesa de Muestra" );
}


int main(){
    //leerImgnMatriz("imagenes/test.jpeg");
    leerImgnMatriz("imagenes/Clase r1_1.jpg");
}
