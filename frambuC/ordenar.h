#include <stdio.h>
#include <omp.h>
//#include "prueba.h"

/*
    Ordenar matriz de NxM por metodo Burbuja en paralelo
    recordar que primero se debe llamar a la funcion defDimension para
    establecer el tamaño del arreglo, luegose ordena la matriz de Nx3 (RGB)
    con las prioridades R,G y B

*/

int dimFil=1;
int dimCol=1;

void defDimension(int a,int b){
    dimFil=a;
    dimCol=b;
}

int burbujaP(int matriz[dimFil][dimCol]){
    int i,j,aux[3];
    for(i=0;i<dimFil-1;i++){
        #pragma omp parallel for schedule(static) private(j)
        for(j=i+1;j<dimFil;j++){
            if(matriz[i][0]>matriz[j][0]){
                aux[0]=matriz[i][0];
                aux[1]=matriz[i][1];
                aux[2]=matriz[i][2];
                matriz[i][0]=matriz[j][0];
                matriz[i][1]=matriz[j][1];
                matriz[i][2]=matriz[j][2];
                matriz[j][0]=aux[0];
                matriz[j][1]=aux[1];
                matriz[j][2]=aux[2];
            }
            else if((matriz[i][0]==matriz[j][0])&&(matriz[i][1]>matriz[j][1])){
                aux[0]=matriz[i][0];
                aux[1]=matriz[i][1];
                aux[2]=matriz[i][2];
                matriz[i][0]=matriz[j][0];
                matriz[i][1]=matriz[j][1];
                matriz[i][2]=matriz[j][2];
                matriz[j][0]=aux[0];
                matriz[j][1]=aux[1];
                matriz[j][2]=aux[2];
            }
            else if((matriz[i][0]==matriz[j][0])&&(matriz[i][1]==matriz[j][1])&&(matriz[i][2]>matriz[j][2])){
                aux[0]=matriz[i][0];
                aux[1]=matriz[i][1];
                aux[2]=matriz[i][2];
                matriz[i][0]=matriz[j][0];
                matriz[i][1]=matriz[j][1];
                matriz[i][2]=matriz[j][2];
                matriz[j][0]=aux[0];
                matriz[j][1]=aux[1];
                matriz[j][2]=aux[2];
            }
        }
    }
    return matriz[dimFil][dimCol];
}

