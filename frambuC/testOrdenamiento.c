/*
    *
    * Programa de comparación de algoritmos de ordenamiento
    * burbuja v/s burbuja en paralelo v/s Quick Sort
    *
    * datos aleatorios entre 1 y 999
    * modo de uso : ./testOrd <cantidad de elementos>
    * compilar : gcc -fopenmp testOrdenamiento.c -o testOrd
    *
    *
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>

int *array, *arrayAux;
int elements;
long time[3];

void bubbleSort();
void parallelBubbleSort();
void bubble();
void parallelBubble();
void timeQuickSort();
void quickSort(int m, int n);
void checkArray();
void showTimes();
void showArray();

int main(int argc, char** argv){
    int i;

    if(argc != 2){
        printf("Modo de uso ./testOrd <nº de valores>\n\n");
        exit(1);
    }
    elements=atoi(argv[1]);
    array=malloc(elements*sizeof(int));
    arrayAux=malloc(elements*sizeof(int));

    printf("Generando arreglo de %d valores...\n\n",elements);
    for(i=0;i<elements;i++)
        array[i]=(rand() % 999) + 1;

    printf("Aplicando Burbuja . . .\n");
    bubbleSort();
    printf("Verificando orden . . .");
    checkArray();

    printf("Aplicando Burbuja en paralelo . . .\n");
    parallelBubbleSort();
    printf("Verificando orden . . .");
    checkArray();

    printf("Aplicando Quick Sort . . .\n");
    timeQuickSort();
    printf("Verificando orden . . .");
    checkArray();

    showTimes();

}


void bubbleSort(){
    int j;
    struct timeval ti,tf;
    long ltiempo;

    for(j=0;j<elements;j++)
        arrayAux[j]=array[j];

    gettimeofday(&ti, NULL);
    bubble(elements);
    gettimeofday(&tf, NULL);
    ltiempo = (tf.tv_sec - ti.tv_sec)*1000000 + tf.tv_usec - ti.tv_usec;
    time[0]= ltiempo/1000.0;

}

void parallelBubbleSort(){
    int j;
    struct timeval ti,tf;
    long ltiempo;

    for(j=0;j<elements;j++)
        arrayAux[j]=array[j];

    gettimeofday(&ti, NULL);
    parallelBubble(elements);
    gettimeofday(&tf, NULL);
    ltiempo = (tf.tv_sec - ti.tv_sec)*1000000 + tf.tv_usec - ti.tv_usec;
    time[1]= ltiempo/1000.0;
}

void timeQuickSort(){
    int j;
    struct timeval ti,tf;
    long ltiempo;

    for(j=0;j<elements;j++)
        arrayAux[j]=array[j];

    gettimeofday(&ti, NULL);
    quickSort(0,elements-1);
    gettimeofday(&tf, NULL);
    ltiempo = (tf.tv_sec - ti.tv_sec)*1000000 + tf.tv_usec - ti.tv_usec;
    time[2]= ltiempo/1000.0;
}

void bubble(){
    int i, changes=1, aux;
    while(changes==1){
        changes=0;
        for(i=0;i<elements-1;i++){
            if(arrayAux[i]>arrayAux[i+1]){
                aux=arrayAux[i];
                arrayAux[i]=arrayAux[i+1];
                arrayAux[i+1]=aux;
                changes=1;
            }

        }
    }

}

void parallelBubble(){
    int i, changes=1, aux;

    while(changes==1){
        changes=0;
        #pragma omp parallel for private (aux)
        for(i=0;i<elements-1;i++){

            if(arrayAux[i]>arrayAux[i+1]){
                #pragma omp atomic read
                aux=arrayAux[i];
                #pragma omp atomic write
                arrayAux[i]=arrayAux[i+1];
                #pragma omp atomic write
                arrayAux[i+1]=aux;
                changes=1;
            }

        }
    }

}

void quickSort(int m, int n ){
    int i,j,k,temp,key;

    if( m < n){
        //elegir el pivote
        k = (m+n)/2;

        temp = arrayAux[m];
        arrayAux[m] = arrayAux[k];
        arrayAux[k] = temp;

        key = arrayAux[m];
        i = m+1;
        j = n;
        while(i <= j){
            while((i <= n) && (arrayAux[i] <= key))
                i++;
            while((j >= m) && (arrayAux[j] > key))
                j--;
            if( i < j){
                temp = arrayAux[i];
                arrayAux[i] = arrayAux[j];
                arrayAux[j] = temp;
            }
        }

        temp = arrayAux[m];
        arrayAux[m] = arrayAux[j];
        arrayAux[j] = temp;


        quickSort(m,j-1);
        quickSort(j+1,n);
    }
}

void showArray(){
    int i;
    printf("\nMostrando Arreglo\n");
    for(i=0;i<elements;i++){
        printf("%d ",arrayAux[i]);
    }
    printf("\n-------------------------------------------------------------\n");
}

void checkArray(){
    int i,check=1;
    for(i=0;i<elements-1;i++){
        if(arrayAux[i]>arrayAux[i+1])
        check=0;
    }
    if(check==1)
        printf(" OK!\n\n");
    else
        printf(" ERROR!    (el arreglo no se encuentra ordenado)\n\n");
}

void showTimes(){
    printf("\n--- Tiempo de orden para algoritmos con %d Datos ---\n",elements);
    printf(" 1. Burbuja             : %ld ms\n",time[0]);
    printf(" 2. Burbuja en paralelo : %ld ms\n",time[1]);
    printf(" 3. Quick Sort          : %ld ms\n\n",time[2]);
}



