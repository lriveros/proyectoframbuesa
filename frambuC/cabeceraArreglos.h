/*
    * Archivo "cabeceraArreglos.h"
    * autor Luis C. Riveros Díaz - Universidad Católica del maule
    * Julio de 2013
    *
    * libreria creada exclusivamente para el proyecto Estimación de madurez de la frambuesa
    * utilizando técnicas de procesamiento de imágenes digitales
    * desarrollado en Agrofreeze LTDA, Talca, Region del Maule, Chile
    *
*/

#include <stdio.h>
#include <omp.h>


int dimFil=1;
int dimCol=1;

void defDimension(int a,int b){
    dimFil=a;
    dimCol=b;
}

int burbujaP(int matriz[dimFil][dimCol]){
    int i,j,aux[3];
    for(i=0;i<dimFil-1;i++){
        #pragma omp parallel for schedule(static) private(j)
        for(j=i+1;j<dimFil;j++){
            if(matriz[i][0]>matriz[j][0]){
                aux[0]=matriz[i][0];
                aux[1]=matriz[i][1];
                aux[2]=matriz[i][2];
                matriz[i][0]=matriz[j][0];
                matriz[i][1]=matriz[j][1];
                matriz[i][2]=matriz[j][2];
                matriz[j][0]=aux[0];
                matriz[j][1]=aux[1];
                matriz[j][2]=aux[2];
            }
            else if((matriz[i][0]==matriz[j][0])&&(matriz[i][1]>matriz[j][1])){
                aux[0]=matriz[i][0];
                aux[1]=matriz[i][1];
                aux[2]=matriz[i][2];
                matriz[i][0]=matriz[j][0];
                matriz[i][1]=matriz[j][1];
                matriz[i][2]=matriz[j][2];
                matriz[j][0]=aux[0];
                matriz[j][1]=aux[1];
                matriz[j][2]=aux[2];
            }
            else if((matriz[i][0]==matriz[j][0])&&(matriz[i][1]==matriz[j][1])&&(matriz[i][2]>matriz[j][2])){
                aux[0]=matriz[i][0];
                aux[1]=matriz[i][1];
                aux[2]=matriz[i][2];
                matriz[i][0]=matriz[j][0];
                matriz[i][1]=matriz[j][1];
                matriz[i][2]=matriz[j][2];
                matriz[j][0]=aux[0];
                matriz[j][1]=aux[1];
                matriz[j][2]=aux[2];
            }
        }
    }
    return matriz[dimFil][dimCol];
}

void quickSort(int m, int n ){
    int i,j,k,temp,key;

    if( m < n){
        //elegir el pivote
        k = (m+n)/2;

        ////swap
        aux[0]=matriz[i][0];
        aux[1]=matriz[i][1];
        aux[2]=matriz[i][2];
        matriz[i][0]=matriz[j][0];
        matriz[i][1]=matriz[j][1];
        matriz[i][2]=matriz[j][2];
        matriz[j][0]=aux[0];
        matriz[j][1]=aux[1];
        matriz[j][2]=aux[2];

        key = matriz[m][0];
        i = m+1;
        j = n;
        while(i <= j){
            while((i <= n) && (matriz[i][0]<= key))
                i++;
            while((j >= m) && (matriz[j][0] > key))
                j--;
            if(matriz[i][0]>matriz[j][0]){
                aux[0]=matriz[i][0];
                aux[1]=matriz[i][1];
                aux[2]=matriz[i][2];
                matriz[i][0]=matriz[j][0];
                matriz[i][1]=matriz[j][1];
                matriz[i][2]=matriz[j][2];
                matriz[j][0]=aux[0];
                matriz[j][1]=aux[1];
                matriz[j][2]=aux[2];
            }
            else if((matriz[i][0]==matriz[j][0])&&(matriz[i][1]>matriz[j][1])){
                aux[0]=matriz[i][0];
                aux[1]=matriz[i][1];
                aux[2]=matriz[i][2];
                matriz[i][0]=matriz[j][0];
                matriz[i][1]=matriz[j][1];
                matriz[i][2]=matriz[j][2];
                matriz[j][0]=aux[0];
                matriz[j][1]=aux[1];
                matriz[j][2]=aux[2];
            }
            else if((matriz[i][0]==matriz[j][0])&&(matriz[i][1]==matriz[j][1])&&(matriz[i][2]>matriz[j][2])){
                aux[0]=matriz[i][0];
                aux[1]=matriz[i][1];
                aux[2]=matriz[i][2];
                matriz[i][0]=matriz[j][0];
                matriz[i][1]=matriz[j][1];
                matriz[i][2]=matriz[j][2];
                matriz[j][0]=aux[0];
                matriz[j][1]=aux[1];
                matriz[j][2]=aux[2];
            }
        }
        ////swap
        aux[0]=matriz[i][0];
        aux[1]=matriz[i][1];
        aux[2]=matriz[i][2];
        matriz[i][0]=matriz[j][0];
        matriz[i][1]=matriz[j][1];
        matriz[i][2]=matriz[j][2];
        matriz[j][0]=aux[0];
        matriz[j][1]=aux[1];
        matriz[j][2]=aux[2];


        quickSort(m,j-1);
        quickSort(j+1,n);
    }
}


