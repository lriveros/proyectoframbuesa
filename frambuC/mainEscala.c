/**
    * Archivo "Main.c"
    * autor Luis C. Riveros Díaz - Universidad Católica del maule
    * Julio de 2013
    *
    * libreria creada exclusivamente para el proyecto Estimación de madurez de la frambuesa
    * utilizando técnicas de procesamiento de imágenes digitales
    * desarrollado en Agrofreeze LTDA, Talca, Region del Maule, Chile
    *
    * instalación de openCV necesaria para compilar
    *
    * comando de instalación
    *       gcc -fopenmp mainEscala.c -o mainEscala `pkg-config --cflags --libs opencv`
    *
*/

#include <stdio.h>
#include "cabeceraOpenCV.h"
#include "cabeceraControl.h"

#define numSegmentos 500
#define OrdenPoli 4   //Es de orden 3 (4 es el numero de coef)ax³+bx²+cx+d

int ** matrixR;
int ** matrixG;
int ** matrixB;
int ** segmentedColors;
int ** repColors;
int * arrayDist;
int rows,cols,nColorSeg;

void getImageData(char* imageName);
void getRepresentativeColors();
void showFinalStats();

main(int argc, char** argv){
    if(argc!=2){
        printf("\n Modo de uso ./main <nombreImagen>\n");
        exit(1);
    }
    printf("\n\t----- Abriendo imagen \"%s\" -----\n",argv[1]);
    getImageData(argv[1]);
    getRepresentativeColors();
}

void getImageData(char* imageName){
    ocvReadImage(imageName);                    // se lee la imagen a estudiar
    ocvThresholdBW();                           // se umbraliza la imagen para obtener una en blanco y negro

    //obteniendo los arreglos R, G y B
    matrixR=ocvGetR();
    matrixG=ocvGetG();
    matrixB=ocvGetB();

    segmentedColors=ocvGetSegmentedColors();    // Se obtiene un arreglo de nx3 con los colores relevantes de la imagen (n=numero de colores)
    nColorSeg=ocvGetNColorSeg();                // se obtiene el numero de colores que compone el arreglo anterior
    repColors=ctrlPartitions(segmentedColors,nColorSeg,numSegmentos);
}

void minimos(){

}
