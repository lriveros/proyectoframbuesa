#!/usr/bin/python
# -*- coding: utf8 -*-
try:
	from Tkinter import *  # Python 2
except ImportError:
	from tkinter import *  # Python 3

import ttk
import ConfigParser
import threading
import utils
import time

root = Tk()
root.title("Evaluador Frambuesa")
root.geometry("256x192+5+5")
t1 = Toplevel(root)
t1.withdraw()

###Variables de vista principal

txtBtnIniciar = StringVar()
txtBtnIniciar.set("Iniciar")

txtReBlock = StringVar()
txtReIqf = StringVar()
txtReClase = StringVar()

txtReBlock.set(" - %")
txtReIqf.set(" - %")
txtReClase.set(" --- ")

fuente = "Courier 10 bold"
valorIqf = int(75)

lblBlock = Label(root, text="% Calidad Block :", font=fuente)
lblReBlock = Label(root, textvariable=txtReBlock, font=fuente)
lblIqf = Label(root, text="% Calidad IQF   :", font=fuente)
lblReIqf = Label(root, textvariable=txtReIqf, font=fuente)
lblClasificacion = Label(root, text="Clasificación   :", font=fuente)
lblReClase = Label(root, textvariable=txtReClase, font=fuente)

pbClasificacion = ttk.Progressbar(orient='vertical', mode='determinate', maximum=100)
pbClasificacion["value"] = valorIqf

#Variables de vista de calibracion
txtOk1 = StringVar()
txtOk2 = StringVar()
txtOk3 = StringVar()

# Valores desde properties
cp = ConfigParser.ConfigParser()
cp.read("config.cfg")
colores = eval(cp.get("AppConf", "colores"), {}, {})
umbralIqf = int(cp.get("AppConf", "umbralIqf"))

# atributos globales
cantidadTotal = [0, 0, 0, 0, 0, 0]
vecTransform = [0, 0, 0]
listaImagenes = []
flagEjecutar = False
flagCalibra = False


def actualizaStats():
	while not flagCalibra:
		time.sleep(2)
	print("::Worker actualiza stats iniciado::")
	global valorIqf
	while flagEjecutar:
		try:
			sumaTot = 0
			sumaIqf = 0
			for i in range(0, len(cantidadTotal)):
				sumaTot += cantidadTotal[i]
				if i > 3:
					sumaIqf += cantidadTotal[i]
			valorIqf = sumaIqf * 100 / sumaTot
			print(str(sumaIqf) + " IQF sobre " + str(sumaTot) + " total -> v_iqf " + str(valorIqf))
		except:
			time.sleep(2)
		time.sleep(2)
	print("::Worker actualiza stats Finalizado::")


def procesoObtenerImagen():
	while not flagCalibra:
		time.sleep(2)
	print("::Worker captura imagenes iniciado::")
	while flagEjecutar:
		if len(listaImagenes) < 10:
			listaImagenes.append(utils.capturarImagen())

		time.sleep(2)
	print("::Worker captura imagenes Finalizado::")


def procesoEvaluadorFram():
	while not flagCalibra:
		time.sleep(2)
	print("::Worker evaluador de frambuesa iniciado::")
	while flagEjecutar:
		if (len(listaImagenes) > 0):
			res = utils.evaluarFrambuesa(listaImagenes[0])
			spr = res.split(";")
			for i in range(0, len(cantidadTotal)):
				cantidadTotal[i] += int(spr[i])
			listaImagenes.remove(listaImagenes[0])
		else:
			time.sleep(2)
		time.sleep(0)
	print("::Worker evaluador de frambuesa Finalizado::")


def refrescarVista():
	while not flagCalibra:
		time.sleep(2)
	txtBtnIniciar.set("Detener")
	print("::Worker que refresca vista iniciado::")
	while flagEjecutar:
		global valorIqf
		txtReIqf.set(str(valorIqf) + " %")
		txtReBlock.set(str(100 - valorIqf) + " %")
		pbClasificacion["value"] = valorIqf
		if (valorIqf >= umbralIqf):
			txtReClase.set("IQF")
		else:
			txtReClase.set("BLOCK")

		root.update()
		time.sleep(2)
	print("::Worker que refresca vista Finalizado::")


def calibrarColores():
	global flagCalibra
	global t1
	## Crea la ventana hija.
	#t1 = Toplevel(root)

	t1.geometry('225x130+20+40')
	t1.wm_title("Calibrando . . .")
	t1.deiconify()
	## Provoca que la ventana tome el focus
	t1.focus_set()
	## Deshabilita todas las otras ventanas hasta que
	## esta ventana sea destruida.
	t1.grab_set()
	## Indica que la ventana es de tipo transient, lo que significa
	## que la ventana aparece al frente del padre.
	t1.transient(master=root)

	txtOk1.set("")
	txtOk2.set("")
	txtOk3.set("")

	lblImgn = Label(t1, text="Imagen calibración.....", font=fuente)
	lblVect = Label(t1, text="Calculando escala......", font=fuente)
	lblCaTo = Label(t1, text="Estado de calibración..", font=fuente)
	lblOkImgn = Label(t1, textvariable=txtOk1, font=fuente)
	lblOkVect = Label(t1, textvariable=txtOk2, font=fuente)
	lblOkCaTo = Label(t1, textvariable=txtOk3, font=fuente)
	lblImgn.place(x=5, y=10)
	lblVect.place(x=5, y=30)
	lblCaTo.place(x=5, y=50)
	lblOkImgn.place(x=185, y=10)
	lblOkVect.place(x=185, y=30)
	lblOkCaTo.place(x=185, y=50)

	btnCalibrar = Button(t1, text="Calibrar", command=calcularTransformacion)

	btnCalibrar.pack(side="bottom", fill="both", expand=False, padx=40, pady=10)
	#t1.grab_set()
	## Pausa el mainloop de la ventana de donde se hizo la invocación.
	t1.wait_window(t1)
	#root.wait_window(t1)


def calcularTransformacion():
	print("Calcuar transformacion")
	global flagCalibra
	global colorOk2
	time.sleep(1)
	txtOk1.set("OK")
	t1.update()
	time.sleep(1)
	txtOk2.set("OK")
	t1.update()
	time.sleep(1)
	txtOk3.set("OK")
	t1.update()
	time.sleep(1)
	t1.destroy()
	flagCalibra = True
	print (" - Fin - ")

def iniciarProceso():
	global flagEjecutar
	global flagCalibra
	if flagEjecutar:
		print("Detener Proceso")
		flagEjecutar = False
		txtBtnIniciar.set("Iniciar")
	else:
		print("Iniciando Proceso")
		txtBtnIniciar.set("Detener")
		if not flagCalibra:
			calibrarColores()
		flagEjecutar = True
		print("volver")
		th1 = threading.Thread(target=procesoObtenerImagen)
		th2 = threading.Thread(target=procesoEvaluadorFram)
		th3 = threading.Thread(target=refrescarVista)
		th4 = threading.Thread(target=actualizaStats)
		th1.start()
		th2.start()
		th3.start()
		th4.start()

btnIniciar = Button(root, textvariable=txtBtnIniciar, command=iniciarProceso)

btnIniciar.pack(pady=15)
lblBlock.place(x=10, y=80)
lblIqf.place(x=10, y=60)
lblClasificacion.place(x=10, y=110)
lblReBlock.place(x=150, y=80)
lblReIqf.place(x=150, y=60)
lblReClase.place(x=150, y=110)
pbClasificacion.place(x=200, y=60)

root.mainloop()
